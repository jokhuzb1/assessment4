import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PunterTest {
	
	private static List<Die> setUpDice() {
        
        Die d1 = new Die();
        Die d2 = new Die();
        Die d3 = new Die();        
        List<Die> dice = new ArrayList<>(Arrays.asList(d1, d2, d3));

        return dice;
    }
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBalanceExceedsLimitBy() {
		 Punter punter = new Punter("Admin", 1000, 20);    
	     assertTrue(punter.balanceExceedsLimitBy(980));//Bug Exists because of this function
	}

	@Test
	public void testPlaceBet() {
		
			
	}

	@Test
	public void testReceiveWinnings() {
		 Punter punter = new Punter("Admin", 1000, 20);
	    
	     
		 int matches = 0;
		 Die d = new Die();
		 d.roll();//Die is rolled
		 int currentBet =200;
		 punter.placeBet(currentBet);//Place a bet of 200
		 Face pick= d.getFace();// forcing a win scenario

		 if(d.getFace().equals(pick)) { 
				matches += 1;
		 }
		 int winnings = matches * currentBet;// increment winning by the current Bet amount

		 if(matches == 1) {	
		    punter.returnBet();
		    assertEquals(1000,punter.getBalance());// verify that punter recieve's  the initial amount
		    
			punter.receiveWinnings(winnings);
			assertEquals(1200,punter.getBalance());// verify that the punter recieve's additional win
		 }
	}

	@Test
	public void testLoseBet() {
		Punter punter = new Punter("Admin", 1000, 20);
	    
	     
		 int matches = 0;
		 Die d = new Die();
		 d.roll();//Die is rolled
		 int currentBet =200;
		 punter.placeBet(currentBet);//Place a bet of 200
		 
		 Face pick= Face.getByIndex(0);
		 
		 matches = 0;//forcing a lose scenario
		 
		 int winnings = matches * currentBet;// increment winning by the current Bet amount

		 if(matches == 1) {	
		    punter.returnBet();
		    assertEquals(1000,punter.getBalance());// verify that punter recieve's  the initial amount
		    
			punter.receiveWinnings(winnings);
			assertEquals(1200,punter.getBalance());// verify that the punter recieve's additional win
		 }
		 else
		 {
			 punter.loseBet();//Losing bet
			 assertEquals(800,punter.getBalance());//verifying that the punter loses the amount bet
		 }
	}

}
